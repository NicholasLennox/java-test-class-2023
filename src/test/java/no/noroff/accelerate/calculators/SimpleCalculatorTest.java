package no.noroff.accelerate.calculators;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleCalculatorTest {
    SimpleCalculator chichu = new SimpleCalculator();



    @Test
    public void add_validInputs_shouldReturnSum() {
        // Arrange
        double num1 = 1;
        double num2 = 1;
        double expected = 1 + 1; // 2 - Magic variable
        SimpleCalculator calculator = new SimpleCalculator();
        // Act
        double actual = calculator.add(num1,num2);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void test1() {
        assertEquals(2,chichu.add(1,1));
    }

    @Test
    public void divide_zeroDenominator_shouldThrowArithmeticExceptionWithAppropriateMessage() {
        // Arrange
        double num1 = 1;
        double num2 = 0;
        String expected = "Cannot divide by zero";
        SimpleCalculator calculator = new SimpleCalculator();
        // Act
        Exception exception = assertThrows(
                ArithmeticException.class, () -> calculator.divide(num1,num2));
        String actual = exception.getMessage();
        // Assert
        assertEquals(expected, actual);
    }
}